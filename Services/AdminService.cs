﻿using ISSEP_Formation_API.Models;

namespace ISSEP_Formation_API.Services
{
    public class AdminService
    {
        public EmployeService EmployeService { get; set; }

        public AdminService(EmployeServiceImpl employeService)
        {
            EmployeService = employeService;
        }

        public List<Employe> Employes => EmployeService.findAll();
    }
}
