﻿using ISSEP_Formation_API.Models;

namespace ISSEP_Formation_API.Services
{
    public interface EmployeService
    {
        List<Employe> findAll();
        Employe FindOneById(int id);

        Employe Insert(Employe employe);
    }

    public class EmployeServiceImpl : EmployeService
    {
        private List<Employe> employeList = new List<Employe>
        {
            new Employe { Id= 1, Firstname= "Flavian", Lastname= "Ovyn"}
        };
        public List<Employe> findAll()
        {
            return employeList;
        }

        public Employe FindOneById(int id)
        {
            return employeList
                .Where(it => it.Id == id)
                .FirstOrDefault();
        }

        public Employe Insert(Employe employe)
        {
            int lastId = employeList.Last().Id;
            employe.Id = lastId + 1;
            employeList.Add(employe);

            return employe;
        }
    }
}
