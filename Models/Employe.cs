﻿using System.ComponentModel.DataAnnotations;

namespace ISSEP_Formation_API.Models
{
    public class Employe
    {
        public int Id { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
    }
}
